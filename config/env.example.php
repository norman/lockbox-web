<?php

$conf_vars = [
    'LB_DL_PASS' => '',
    'LB_FILE' => '/var/www/data/lockbox.dat', // This is the submissions file -- it should not be in your web directory!!!
    'LB_PUBKEY_FILE' => '/var/www/html/key.public' // Absolute parth of public key file
];

// restrict the (optional) admin form to these IPs
$ips = array(
/*  "127.0.0.1",
    "192.168.0.0", 
    "172.16.0.0" */
);

?>
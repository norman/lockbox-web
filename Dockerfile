FROM php:7.4-apache

# Set working directy at web server root
WORKDIR /var/www/html

# Manually installs Lockbox
COPY ./lockbox/ .
COPY ./config/env.example.php ./php/env.php
COPY ./config/key.public .

# Create directory to save submissions and adjust permissions
RUN mkdir /var/www/data
RUN chown -R www-data:www-data /var/www/data
<?php

// Load private key
$privKey = base64_decode(file_get_contents("key.private"));
$pubKey = base64_decode(file_get_contents("key.public"));
$keypair = sodium_crypto_box_keypair_from_secretkey_and_publickey($privKey, $pubKey);

// Load submissions from encrypted file
$encrypted_submissions = explode("\n", file_get_contents("submissions.dat"));

foreach ($encrypted_submissions as $encrypted) {
	if (trim($encrypted)=="") continue;

	$parts = explode("|", $encrypted);

	$decryptedData = sodium_crypto_box_seal_open(base64_decode($parts[1]), $keypair);

	// Parse exported variable
	$vars = json_decode($decryptedData, true);

	// Print :)
	foreach ($vars as $k => $v){
		echo "$k: $v\n";
	}
	echo "\n";
}

?>	

![](lockbox/img/logo.png)

This is the PHP/webserver component for Lockbox. For instructions visit [the Lockbox app`s main repository](https://git.openprivacy.ca/openprivacy/lockbox).


## Docker Install Instructions
Right now, there is a very basic process to building and deploying a Docker image of Lockbox in a container that contains php and an instance of apache webserver. The image has been tested on linux base OS and will need to be run of a host that already has the OS installed. Future development will aim to have a more robust install.

Note: This version of the Docker deployment is using an `env.php` file instead of environment variables due to some of the nuances with how php handles environment variables.

The `.docker` directory contains a Dockerfile to build an image for your deployment. Do the following before building the image:
* Modify `config/env.example.php` based on any customizations
* Generate a keypair (you can use the script in the `cmd` folder (`genkeys.php.txt`, which needs to be renamed to `genkeys.php` prior to use, or use a key pair you have already generated) and place the public key in the `config` folder as `key.public`
<!DOCTYPE html>
<html>
<head>
	<title><?= MSG_TITLE ?></title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/custom.css">
</head>
<body>

<div class="container">

	<div class="titlebar">
		<img src="img/logo.png" alt="logo" class="logo">
		<h1 class="title"><?= MSG_TITLE ?></h1>
		<div style="clear:both"></div>
	</div>
	

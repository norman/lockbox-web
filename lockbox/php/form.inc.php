<p>Welcome to Lockbox by Open Privacy! To get started, replace the contents of this form in <tt>php/form.inc.php</tt> with your own. If you know HTML you can write the form yourself. Otherwise you can use any form building tool that outputs to HTML.</p>

<form method="post">

<h2>Application Questions</h2>

<input type="text" name="email" style="display:none">

<p>1. What is your name (preferred or legal)?</p>
<input type="text" name="01_name">

<p>2. What city/neighbourhood do you live in?</p>
<input type="text" name="02_where">

<p>3. Is there anything else you'd like us to know?</p>
<textarea name="03_misc"></textarea>

<p>4. What is your favourite flavour?</p>
<select name="04_fav">
	<option>Strawberry</option>
	<option>Lettuce</option>
	<option>Shoelace</option>
	<option>Other</option>
</select>

<p>5. Pick one?</p>
<label><input type="radio" name="05_pick" value="10-12"> 10-12</label>
<label><input type="radio" name="05_pick" value="13+"> 13+</label>
<label><input type="radio" name="05_pick" value="25 and a pickle"> 25 and a pickle</label>

<p>Please remember to click the Submit button below once you are ready.</p>

<script type="text/javascript">
function h(cb) {
	const btn = document.getElementById("btnsubmit");
	if (cb.checked) {
		btn.disabled = false;
		btn.classList.remove('button-disabled');
		btn.classList.add('button-primary');
	} else {
		btn.disabled = true;
		btn.classList.remove('button-primary');
		btn.classList.add('button-disabled');
	}
}
</script>
<br>
<label>
	<input type="checkbox" name="09_agree" onchange="h(this)">
	I acknowledge that everything in this form is true to the best of my knowledge.
</label>
<br>

<input type="submit" class="button-disabled" id="btnsubmit" value="Submit" disabled>

</form>
